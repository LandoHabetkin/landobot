<?php

$config = array();
/*
 * Bot configuration
 */


/*
 * If this is set to true, the bot will act as a jabber relay.
 * He will NOT listen to messages from discord
 */
$config["jabberRelay"] = true;

/*
 * Jabber configuration
 * server: jabber server to join
 * port: 5222 is default
 * user: jabber username (the part before the @)
 * password: jabber passowrd
 */
$config["jabber"] = array("server" => "",
    "port" => "",
    "user" => "",
    "password" => "");



/*
 * If jabber relay is false the bot will accept commands in discord from these people
 */
$config["botAdmins"] = array("Lando Habetkin");
/*
 * Discord configuration
 * email: discord email
 * password: discord password
 * guilds: list of server IDs to be joined
 * channels: list of channel IDs to be joined
 *   chat: these are the channels the bot will speak in
 *   control: these are the channels the bot will accept commands from
 */


$config["discord"] = array("email" => "",
    "password" => "",
    "servers" => [""],
    "channels" => [
        "chat" => [""],
        "control" => [""]
    ]
);

