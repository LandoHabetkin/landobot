﻿<?php
error_reporting(E_ERROR);
//require dependencies
require_once(__DIR__ . "/vendor/autoload.php");
// include the configuration file
require("./config/config.php");
// include cURL
require("./include/cURL.php");
// Autoload requires classes on new class()
function myAutoload($class_name)
{
    $path = __DIR__ . "/include/class." . $class_name . ".php";
    if (file_exists($path)) {
        require_once($path);
    } else {
        return false;
    }
}
spl_autoload_register('myAutoload');

/*
convert a JSON object to an Array
*/
function objectToArray($item)
{
    if (is_object($item)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $item = get_object_vars($item);
    }

    if (is_array($item)) {
        /*
        * Return array converted to object
        */
        return array_map(__FUNCTION__, $item);
    } else {
        // Return array
        return $item;
    }
}
?>