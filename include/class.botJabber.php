﻿<?php

/*
* Class to implement a jabber relay
* this connects to a jabber server, listens to messages, and relays them to discord
* @author Lando Habetkin
*
*/

class botJabber
{
    //the discord server object
    var $discord;
    //the last jabber message received
    var $lastJabberMessage;

    function __construct($discord, $channels, $config)
    {
        //include Jabber/XMPP library
        require_once("./include/XMPPHP/XMPP.php");
        $this->discord = $discord;
        $this->channels = $channels;
        $this->config = $config["jabber"];
    }


    function connect()
    {
        //store username or false if not connected
        $connectedAs = false;
        //create connection object
        $jabberConnection = new \XMPPHP\XMPP($this->config["server"], $this->config["port"], $this->config["user"], $this->config["password"], '', '', $printlog = true, $loglevel = \XMPPHP\Log::LEVEL_WARNING);
        //auto subscribe to channel list
        $jabberConnection->autoSubscribe();

        try {
            //connect to jabber server
            $jabberConnection->connect();
            //save the last message
            //$lastMsg = "";

            //connection is established
            while (!$jabberConnection->isDisconnected()) {
                //process server events
                $jabberEvents = $jabberConnection->processUntil(array('message', 'presence', 'end_stream', 'session_start', 'vcard'));

                foreach ($jabberEvents as $event) {
                    $jabberEvent = $event[0];
                    //get the message text
                    $jabberMessage = $event[1];

                    if ($jabberEvent == "message") {
                        //process the message
                        $this->onMessage($jabberMessage, $this->channels["chat"][0]);
                        //tell jabber we are still here
                        $jabberConnection->presence($status = "afk");

                    } elseif ($jabberEvent == "presence") {
                        //send a presence message back if we werent connected before
                        if (!$connectedAs) {
                            echo "Connected as: " . $jabberConnection->fulljid . "\n";
                            $this->discord->api("channel")->messages()->create($this->channels["control"][0], "**Connected as:** " . $jabberConnection->fulljid);
                            $jabberConnection->presence($status = "afk");
                            $connectedAs = $jabberConnection->fulljid;
                        }

                        echo "Presence: {$jabberMessage['from']} [{$jabberMessage['show']}] {$jabberMessage['status']}\n";
                    } elseif ($jabberEvent == "session_start") {
                        echo "Connected to: " . $this->config["server"] . "\n";
                        //get userlist
                        $jabberConnection->getRoster();
                        //tell jabber we are still here
                        $jabberConnection->presence($status = "afk");
                    }
                }
            }
        } catch (XMPPHP_Exception $exception) {
            die($exception->getMessage());
        }
    }

    /*
    * This is called in the main event loop on each message
    * It parses the message and relays it to discord
    * @author Lando Habetkin
    *
    */
    function onMessage($message, $channelID)
    {
        //a message from jabber was received
        echo "---------------------------------------------------------------------------------\n";
        echo "Message from: {$message['from']}\n";
        echo $message['body'] . "\n";
        echo "---------------------------------------------------------------------------------\n";

        if (!empty($message["body"])) {
            //only process a message for discord once
            if ($message["body"] != $this->lastJabberMessage) {
                //save last message for comparing later, and not send message twice
                $this->lastJabberMessage = $message["body"];

                //copy jabber message body, to replace some stuff
                $chatBody = $message["body"];

                //replace some stuff
                $chatBody = str_ireplace("broadcast to all", "", $chatBody);
                $chatBody = str_ireplace("Replies to this message are not monitored", "", $chatBody);
                $chatBody = str_ireplace("*", "", $chatBody);
                $chatBody = str_ireplace("to [all]", "", $chatBody);
                $chatBody = str_ireplace("||", "\n", $chatBody);
                $chatBody = str_ireplace("@", "\n", $chatBody);

                //send to discord
                $this->discord->api("channel")->messages()->create($channelID, "@everyone \n**Coalition Ping**" . $chatBody);
            }
        }
    }
}
