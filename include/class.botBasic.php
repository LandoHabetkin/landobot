﻿<?php
/*
* Class to implement basic bot functionality
* @author Lando Habetkin
*
*/

class botBasic {
    var $discord;

    function __construct($discord)
    {
        $this->discord = $discord;
    }

    /*
    * This is called in the main event loop on each message
    * It parses the message for triggers, fetches the data from ZKill and processes it
    * @author Lando Habetkin
    *
    */
    function onMessage($message, $channelID)
    {
        // print program help
        if (stristr($message, "!help")) {
            $this->discord->api("channel")->messages()->create($channelID, "###COMMANDS###
                                !ishtarloss -  Show the last ishtar loss
                                !lastkills  -  Show the 5 most recent kills
                                !help      -   Show this help message
                                !jita, amarr, hek, rens <item> - Show the prices for <item>
                                !info     -    Show version number etc
                                !shutdown  -   End the program
                                ");
        }
        // shutdown the program
        if (stristr($message, "!shutdown")) {
            $this->discord->api("channel")->messages()->create($channelID, "[Shutting down...]");
            die();
        }
        // show the program version etc
        if (stristr($message, "!info")) {
            $this->discord->api("channel")->messages()->create($channelID, "[LandosBot V1.1]");
        }
        // show the program version etc
        if (stristr($message, "!urban")) {
            // convert message to array
            $explodeMsg = explode(" ", $message);
            // remove first entry, i.e. the command
            array_shift($explodeMsg);
            // assemble the query string
            $queryString = "";
            foreach($explodeMsg as $msgPart) {
                $queryString .= trim($msgPart) . " ";
            }
            // get the JSON from urban dictionary
            $ubJSON = objectToArray(json_decode(file_get_contents("http://api.urbandictionary.com/v0/define?term=" . urlencode($queryString))));
            // $ubJSON = downloadData("http://api.urbandictionary.com/v0/define?term=" . $queryString);
            // get definition and example text
            $definition = $ubJSON["list"][0]["definition"];
            $example = $ubJSON["list"][0]["example"];

            $definition = substr($definition,0,1800);
            $example = substr($example,0,1800);
            // send to chat
            $this->discord->api("channel")->messages()->create($channelID, "**Definition:** $definition\n**Example:** $example");
        }
    }
}
