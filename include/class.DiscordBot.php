<?php
use Discord\Discord;

/*
 * Basic object for the bot
 * This holds configuration, the discord server objects and the main application loop
 * It also manages feature modules implemented as objects
 */

class DiscordBot
{
    // configuration
    var $config;
    //discord configuration
    var $configDiscord;
    // discord server object
    var $discord;
    // id of the last message received
    var $oldMsgID = array();
    // content of the last message received
    var $oldMsg = array();
    // server IDs to connect to
    var $servers;
    var $channels;
    var $serverChannels;
    // basic bot features object
    var $botBasic;
    // zkill Stats object
    var $zkillStats;
    // admins
    var $admins;

    /*
    * Constructor
    * @param array $config Program configuration
    * @return void
    */
    function __construct($config)
    {
        // assign config to member variable
        $this->config = $config;
        $this->configDiscord = $config["discord"];
        $this->admins = $config["botAdmins"];

        // connect to the discord
        $this->discord = new Discord($this->configDiscord["email"], $this->configDiscord["password"]);
        // read the server IDs from the config
        $this->servers = $config["discord"]["servers"];
        // loop thru the servers so we can connect several servers if we want
        foreach ($this->servers as $serverID) {
            // get the channels for the server
            $channelList = $this->discord->api("guild")->channels()->show($serverID);
            // add the channels to the serverChannels field
            $this->serverChannels = [];
            foreach ($channelList as $channelData) {
                array_push($this->serverChannels, ["id" => $channelData["id"], "name" => $channelData["name"]]);
            }
        }
        $this->channels = $this->configDiscord["channels"];

        // initialize bot feature modules
        //basic bot commands
        $this->botBasic = new botBasic($this->discord);
        //zkill features
       // $this->zkillStats = new zkillStats($this->discord);
        //jabbber ping relay feature
        if ($config["jabberRelay"]) {
            //jabber
            $this->jabber = new botJabber($this->discord, $this->channels, $config);
        }
    }

    /*
    * Main function handling the main application loop
    * @return void
    */
    function main()
    {
        if ($this->config["jabberRelay"]) {
            echo "Connecting to jabber\n";
            $this->jabber->connect();
        } else {
            echo "Listening to discord\n";
            $this->parseDiscord();
        }
    }

    /*
    Function to parse messages
    * @param int $channelID ID of the channel to send back to
    * @param string $message The message to parse
    * @return void
    */
    function parseMessage($message, $channelID)
    {
        $ticks = 0;
        // parse for basic bot commands
        $this->botBasic->onMessage($message, $channelID);
        // parse for zkillStats features
        //$this->zkillStats->onMessage($message, $channelID);
    }

    function parseDiscord()
    {
        $this->discord->api("channel")->messages()->create($this->channels["chat"][1], "[Lando Bot starting]");
        // endless loop so the bot keeps listening
        //$ticks = 0;
        $messages = [];
        while (true) {
            try {
                // loop thru the channels and read their channel messages
                foreach ($this->channels["chat"] as $key => $channel) {
                    // read the messages from the discord API - only get the messages since the last message
                    $messages[$key] = $this->discord->api("channel")->messages()->show($channel, 1, null, isset($this->oldMsgID[$key]) ? $this->oldMsgID[$key] : null);
                }
                // loop thru messages
                foreach ($messages as $key => $messageBody) {
                    // loop thru message bodies
                    foreach ($messageBody as $message) {
                        // Check if the message is new
                        if (!isset($this->oldMsg[$key]) || ($this->oldMsg[$key] != $message["content"])) {
                            // update last messageID to the current messageID
                            $this->oldMsgID[$key] = $message["id"];

                            // get the channel ID and message string
                            $channelID = $message["channel_id"];
                            $messageText = $message["content"];
                            $user = $message["author"]["username"];
                            // print the chat text to the CLI window
                            echo "DISCORD: " . $user . ":" . $messageText . "\n";
                            //clear the buffer
                            flush();

                            // parse the chat messages, react to them etc
                            if (in_array($user, $this->admins) and !$this->config["jabberRelay"]) {
                                // parse messages for commands
                                $this->parseMessage($messageText, $channelID);
                            }

                            // update the last message content with the current message
                            $this->oldMsg[$key] = $message["content"];
                        }
                    }
                }
            } catch (Exception $e) {
                var_dump($e->getMessage());
            }
        }

        //$ticks = $ticks + 1;
        //wait 1s
        sleep(1);
    }
}
